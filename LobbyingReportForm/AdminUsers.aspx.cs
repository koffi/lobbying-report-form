﻿using LobbyingReportForm.DBACCESS;
using System;
using System.Collections.Generic;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LobbyingReportForm.STRUCTURES;

namespace LobbyingReportForm
{
    public partial class AdminUsers : System.Web.UI.Page
    {
        User TheUser = new User();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Context.User.Identity.IsAuthenticated)
            {
                Login.LdapDisconnect();
                Response.Redirect("Login.aspx");
            }

            TheUser = (new Entities()).GetUserByCCID(Context.User.Identity.Name);

            LbUserName.Text = TheUser.FirstName + " " + TheUser.LastName;
            if(!IsPostBack)
            { 
                loadusers();
            }
            

            DdlUserRole.DataSource = (new Entities()).Role_GetAll();
            DdlUserRole.DataValueField = "ID";
            DdlUserRole.DataTextField = "Name";
            DdlUserRole.DataBind();
        }

        public void loadusers()
        {
            GridViewUsers.DataSource = (new Entities()).Users_GetAll();
            GridViewUsers.DataBind();
        }

        protected void LbDownload_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdminDownload.aspx");
        }

        protected void LbUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("Adminusers.aspx");
        }

        protected void LbLogout_Click(object sender, EventArgs e)
        {
            Login.LdapDisconnect();
            Response.Redirect("Login.aspx");
        }

        private static String GetEntryValue(SearchResultEntry entry, String attribute)
        {
            return String.Join(",", entry.Attributes[attribute].GetValues(attribute.GetType()));
        }

        protected void BtAddCCID_Click(object sender, EventArgs e)
        {
            //find ccid and add it

            User theUser = (new Entities()).GetUserByCCID(TbCCID.Value);

            if (theUser.CCID != "NOTFOUND")
            {
                LblError.Text = "This user already exists!";
            }
            else
            {
                SearchResultEntry data = Login.GetLdapUser(TbCCID.Value);

                if (data == null)
                {
                    LblError.Text = "User not found in LDAP. Please contact administrator.";
                }
                else
                {

                    User NewUser = new User();

                    if (data.Attributes.Contains("givenname"))
                    {
                        NewUser.FirstName = GetEntryValue(data, "givenname");
                    }
                    if (data.Attributes.Contains("sn"))
                    {
                        NewUser.LastName = GetEntryValue(data, "sn");
                    }
                    if (data.Attributes.Contains("mail"))
                    {
                        NewUser.Email = GetEntryValue(data, "mail");
                    }
                    if (data.Attributes.Contains("title"))
                    {
                        NewUser.Title = GetEntryValue(data, "title");
                    }
                    if (data.Attributes.Contains("telephonenumber"))
                    {
                        NewUser.Phone = GetEntryValue(data, "telephonenumber");
                    }
                    if (data.Attributes.Contains("ou"))
                    {
                        NewUser.Phone = GetEntryValue(data, "Organization-Name");
                    }
                    if (data.Attributes.Contains("department"))
                    {
                        NewUser.Phone = GetEntryValue(data, "department");
                    }
                    NewUser.Status = 1;
                    NewUser.CCID = TbCCID.Value.ToLower();
                    NewUser.Role = Convert.ToInt16(DdlUserRole.SelectedValue);

                    (new Entities()).InsertUser(NewUser);

                    loadusers();
                }

            }
        }

        protected void BtDisable_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridViewUsers.Rows)
            {

                if (((CheckBox)row.FindControl("UserSelector")).Checked)
                {
                    (new Entities()).DisableUser(row.Cells[1].Text.ToLower());
                }
                

            }
            loadusers();
        }

        protected void BtEnable_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridViewUsers.Rows)
            {

                if (((CheckBox)row.FindControl("UserSelector")).Checked)
                {
                    (new Entities()).EnableUser(row.Cells[1].Text.ToLower());
                }

            }
            loadusers();
        }
    }
}