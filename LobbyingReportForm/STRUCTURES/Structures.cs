﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LobbyingReportForm.STRUCTURES
{
    public struct GovOrder
    {
        private string _value;
        private string _label;

        public string value
        {
            get { return _value; }
            set { _value = value; }
        }

        public string label
        {
            get { return _label; }
            set { _label = value; }
        }
    }

    public struct ComMethod
    {
        private string _label;
        private string _data;

        public string label
        {
            get { return _label; }
            set { _label = value; }

        }

        public string data
        {
            get { return _data; }
            set { _data = value; }
        }
    }

    public struct Response
    {
        private DateTime _Date;
        private int _ComMethodID;
        private int _GovOrderID;
        private string _GovOfficialName;
        private string _GovOfficialDept;
        private string _GovPosition;
        private string _SubjectMatter;
        private string _UofAMeetingParticipant;
        private string _ExternalMeetingParticipant;
        private string _MeetingRequestedBy;
        private string _Comments;
        private int _UserID;


        public DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }

        }

        public int ComMethodID
        {
            get { return _ComMethodID; }
            set { _ComMethodID = value; }
        }

        public int GovOrderID
        {
            get { return _GovOrderID; }
            set { _GovOrderID = value; }
        }

        public string GovOfficialName
        {
            get { return _GovOfficialName; }
            set { _GovOfficialName = value; }
        }

        public string GovOfficialDept
        {
            get { return _GovOfficialDept; }
            set { _GovOfficialDept = value; }
        }

        public string GovPosition
        {
            get { return _GovPosition; }
            set { _GovPosition = value; }
        }

        public string SubjectMatter
        {
            get { return _SubjectMatter; }
            set { _SubjectMatter = value; }
        }


        public string UofAMeetingParticipant
        {
            get { return _UofAMeetingParticipant; }
            set { _UofAMeetingParticipant = value; }
        }

        public string ExternalMeetingParticipant
        {
            get { return _ExternalMeetingParticipant; }
            set { _ExternalMeetingParticipant = value; }
        }

        public string MeetingRequestedBy
        {
            get { return _MeetingRequestedBy; }
            set { _MeetingRequestedBy = value; }
        }

        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }


    }

    public struct LogOnModel
    {

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }

        public string Status { get; set; }

        public bool RememberMe { get; set; }


        public string UppercaseFirstLetter(string value)
        {
            //
            // Uppercase the first letter in the string this extension is called on.
            //
            if (value.Length > 0)
            {
                char[] array = value.ToCharArray();
                array[0] = char.ToUpper(array[0]);
                return new string(array);
            }
            return value;
        }

        //public string UserFullName
        //{
        //    get
        //    {
        //         FCEntities DBContext = new FCEntities();
        //         return DBContext.FC_user.Where(f => f.ccid == this.UserName).Select(s => s.firstName ?? "" + " " + s.lastName ?? "").FirstOrDefault();  
        //    }
        //    set{ }  

        //}


    }

    public struct User
    {
        private string _FirstName;
        private string _Lastname;
        private string _Title;
        private string _CCID;
        private string _Phone;
        private string _Email;
        private int _Role;
        private int _Status;
        private int _UserID;


        public int Role
        {
            get { return _Role; }
            set { _Role = value; }
        }

        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        public string CCID
        {
            get { return _CCID; }
            set { _CCID = value; }
        }
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        public string LastName
        {
            get { return _Lastname; }
            set { _Lastname = value; }
        }

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

    }

}