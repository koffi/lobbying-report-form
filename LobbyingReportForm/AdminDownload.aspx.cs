﻿using LobbyingReportForm.DBACCESS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.IO;
using LobbyingReportForm.STRUCTURES;
using System.Data;

namespace LobbyingReportForm
{
    public partial class Admin : System.Web.UI.Page
    {
        User TheUser = new User();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Context.User.Identity.IsAuthenticated)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                DdlComMethod.DataSource = (new Entities()).ComMethod_GetAll_forDropDown();
                DdlComMethod.DataValueField = "ComMethodId";
                DdlComMethod.DataTextField = "ComMethodName";
                DdlComMethod.DataBind();


                DdlGovOrder.DataSource = (new Entities()).GovOrder_GetAll_forDropDown();
                DdlGovOrder.DataValueField = "GovOrderId";
                DdlGovOrder.DataTextField = "GovOrderName";
                DdlGovOrder.DataBind();
            }

            TheUser = (new Entities()).GetUserByCCID(Context.User.Identity.Name);
            LbUserName.Text = TheUser.FirstName + " " + TheUser.LastName;

            //LoadGridData();
            ButtonDownload.Visible = false;
        }

        protected void LbDownload_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdminDownload.aspx");
        }

        protected void LbUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdminUsers.aspx");
        }

        protected void GridViewResponses_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewResponses.PageIndex = e.NewPageIndex;
            LoadGridData();
        }

        public void LoadGridData()
        {
            DataTable res = (new Entities()).Result_GetAll();

            if (res.Rows.Count == 0)
            {
                lbError.Text = "No record found.";
            }
            else
            {
                GridViewResponses.DataSource = res;
                GridViewResponses.DataBind();
            }
        }

        protected void ButtonDownload_Click(object sender, EventArgs e)
        {
            DataTable results = (new Entities()).Result_GetAll(TbCCID.Value, Convert.ToDateTime(TbStartDate.Value), Convert.ToDateTime(TbEndDate.Value), Convert.ToInt16(DdlComMethod.SelectedValue), Convert.ToInt16(DdlGovOrder.SelectedValue));

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Lobbying Form Report");
            var totalCols = results.Columns.Count;
            var totalRows = results.Rows.Count;

            for (var col = 1; col <= totalCols; col++)
            {
                workSheet.Cells[1, col].Value = results.Columns[col - 1].ColumnName;
            }
            for (var row = 1; row <= totalRows; row++)
            {
                for (var col = 0; col < totalCols; col++)
                {
                    workSheet.Cells[row + 1, col + 1].Value = results.Rows[row - 1][col];
                }
            }
            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=LobbyingFormReport.xlsx");
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }

        protected void LbLogout_Click(object sender, EventArgs e)
        {
            Login.LdapDisconnect();
            Response.Redirect("Login.aspx");
        }

        protected void ButtonDisplay_Click(object sender, EventArgs e)
        {

            DataTable res = (new Entities()).Result_GetAll(TbCCID.Value,Convert.ToDateTime(TbStartDate.Value),Convert.ToDateTime(TbEndDate.Value),Convert.ToInt16(DdlComMethod.SelectedValue),Convert.ToInt16(DdlGovOrder.SelectedValue));

            if (res.Rows.Count == 0)
            {
                lbError.Text = "No record found.";
                GridViewResponses.DataSource = res;
                GridViewResponses.DataBind();
            }
            else
            {
                GridViewResponses.DataSource = res;
                GridViewResponses.DataBind();
                ButtonDisplay.Text = "Refresh Results";
                ButtonDownload.Visible = true;
            }

        }

        protected void GridViewResponses_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



    }
}