﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LobbyingReportForm.DBACCESS;
using LobbyingReportForm.STRUCTURES;

namespace LobbyingReportForm
{
    public partial class Submitted : System.Web.UI.Page
    {
        User TheUser = new User();

        protected void Page_Load(object sender, EventArgs e)
        {
            TheUser = (new Entities()).GetUserByCCID(Context.User.Identity.Name);
            LbUserName.Text = TheUser.FirstName + " " + TheUser.LastName;
        }

        protected void ButtonLogout_Click(object sender, EventArgs e)
        {
            Login.LdapDisconnect();
            Response.Redirect("/Login.aspx");
        }

        protected void ButtonNext_Click(object sender, EventArgs e)
        {
            Response.Redirect("LobbyingReportForm.aspx");
        }
    }
}