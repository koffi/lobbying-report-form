﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Security.Principal;
using System.Threading;
using System.DirectoryServices.Protocols;
using System.DirectoryServices;
using System.Net;
using LobbyingReportForm.STRUCTURES;
using LobbyingReportForm.DBACCESS;

namespace LobbyingReportForm.Account
{

    public class Account
    {

        // GET: /Account/LogOn

        #region LDAP Variables


        private const int _LdapPort = 389;
        private const string _LdapHost = "directory.srv.ualberta.ca";
        private const string _LdapSearchBase = "ou=people,dc=ualberta,dc=ca";
        private static string[] _LdapAttributes = new string[] { "uid", "cn", "sn", "mail", "title", "telephonenumber", "givenname" };
        private static LdapConnection _LdapConn = null;
        private const int _LdapSecurePort = 636;
        private static bool BindResult { get; set; }
        private static string UserPassword { get; set; }
        private static string UserDn { get; set; }
        private static string ErrorMessage { get; set; }


        #endregion

        #region Ldap Connection

        /// <summary>
        /// Creates a connection to the Ldap service
        /// </summary>
        /// <returns>True if connection was made or exists, False if connection was unable to be made</returns>
        public static bool LdapConnect()
        {
            bool result = true;
            if (_LdapConn == null)
            {
                try
                {
                    _LdapConn = new LdapConnection(new LdapDirectoryIdentifier(_LdapHost, _LdapPort));
                    _LdapConn.AuthType = AuthType.Basic;
                    _LdapConn.Bind();
                }
                catch (Exception e)
                {
                    result = false;
                    throw (e);
                }
            }
            return result;
        }

        /// <summary>
        /// Disconnects the current Ldap Connection
        /// </summary>
        /// <returns>True if the connection was disconnected, False if the connection was unable to be disconnected.</returns>
        public static bool LdapDisconnect()
        {
            bool result = true;
            if (_LdapConn != null)
            {
                try
                {
                    _LdapConn.Dispose();
                    _LdapConn = null;
                }
                catch (Exception e)
                {
                    result = false;
                    throw (e);
                }
            }
            return result;
        }

        #endregion

        // POST: /Account/LogOn
 
        public Boolean LogOn(LogOnModel model)
        {
               LogOnModel UserRole = (new Entities()).GetUserRole(model.UserName);



                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);




                        if (model.Role == "1")
                        {

                            if (UserRole.Role == "1")
                            {
                                return RedirectToAction("Index", "Checklists");
                            }
                            else
                            {
                                ModelState.AddModelError("", "Access Denied. Admin Privilege required!");
                                FormsAuthentication.SignOut();
                                return View(model);
                            }
                        }
                        else if (model.Role == "2")
                        {
                                   
                            if (UserRole.Role == "2" || UserRole.Role == "1")
                            {                     
                                return RedirectToAction("ChecklistManager", "Checklists");
                            }
                            else
                            {
                                ModelState.AddModelError("", "Access Denied. SFO Privilege required!");
                                FormsAuthentication.SignOut();
                                return View(model);
                            }
                        }
                        else
                            return RedirectToAction("ChecklistPublic", "Checklists");


        }



        private void DoAuthentication()
        {
            BindResult = false;

            try
            {
                var ldapConn = new LdapConnection(new LdapDirectoryIdentifier(_LdapHost, _LdapSecurePort), new System.Net.NetworkCredential(UserDn, UserPassword), AuthType.Basic);
                ldapConn.SessionOptions.SecureSocketLayer = true;
                ldapConn.Bind();
                ldapConn.Dispose();

                BindResult = true;
            }
            catch (LdapException x)
            {
                throw (x);
            }
            catch (Exception x)
            {
                throw (x);
            }
        }


        // GET: /Account/LogOff
        public void LogOff()
        {
            FormsAuthentication.SignOut();

            //return RedirectToAction("LogOn", "Account");
        }

        //public bool ValidateUser(string username, string password)
        //{

        //    if (Session["loginCount"] == null) //setup the session var with 0 count
        //    {
        //        Session.Add("loginCount", 0);
        //    }

        //    try
        //    {
        //        var theDBctx = new FCEntities(); //class derived from DbContext
        //        var currentuser = theDBctx.FC_user.Where(c => c.ccid == username);


        //        if (currentuser == null || currentuser.Count() == 0)
        //        {
        //            ErrorMessage = "login unknown.";
        //            return false;
        //        }
        //        else
        //        {
        //            var theUser = currentuser.First();
        //            if (theUser != null)
        //            if (theUser.statusID == 2) { 
        //                ErrorMessage = "The user is locked. Please contact the administrator.";
        //                return false;
        //            }
        //        }


        //        SearchResultEntry UserData = GetLdapUser(username);

        //        if (UserData != null)
        //        {
        //            UserDn = UserData.DistinguishedName;
        //            UserPassword = password;
        //            DoAuthentication();
        //        }



        //    }
        //    catch (ThreadAbortException)
        //    {
        //        // Do nothing - user was redirected in Authentication.cs
        //        return false;
        //    }
        //    catch (Exception x)
        //    {
        //       // throw (x); 
        //        //if (x.Message == "The supplied credential is invalid.")
        //        //{
        //        //    ErrorMessage = "The user name or password provided is incorrect.";

        //        //    Session["loginCount"] = (int)Session["loginCount"] + 1;
                                    

        //        //    if ((int)Session["loginCount"] == 6)
        //        //    {   var theDBctx = new FCEntities();
        //        //        var theUser = (from c in theDBctx.FC_user
        //        //                       where c.ccid == username
        //        //                       select c).FirstOrDefault();

        //        //        if (theUser != null)
        //        //        {
                        
        //        //            theUser.statusID = 2;
        //        //            theDBctx.SaveChanges();
        //        //            ErrorMessage = "Too many failed attempts. User account locked.";
        //        //        }
        //        //    }
        //        }
        //        return false;
        //    }
            




        public static SearchResultEntry GetLdapUser(string ccid)
        {
            SearchResultEntry data = null;
            try
            {
                bool createConn = false;
                if (_LdapConn == null)
                {
                    if (LdapConnect())
                    {
                        createConn = true;
                    }
                    else
                    {
                        throw new Exception("LdapConnect Error: Unable to created connection to Ldap.");
                    }
                }
                SearchRequest ldapSearchReq = new SearchRequest(_LdapSearchBase, string.Format("uid={0}", ccid), SearchScope.OneLevel, _LdapAttributes);
                SearchResponse ldapSearchResp = (SearchResponse)_LdapConn.SendRequest(ldapSearchReq);
                if (ldapSearchResp.Entries.Count > 0)
                {
                   
                    data = ldapSearchResp.Entries[0];
                }
                if (createConn)
                {
                    LdapDisconnect();
                }
            }
            catch (Exception e)
            {
                throw(e);
            }
            return data;
        }

    }
}
