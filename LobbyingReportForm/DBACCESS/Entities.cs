﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LobbyingReportForm.STRUCTURES;
using System.Web.Script.Serialization;

namespace LobbyingReportForm.DBACCESS
{

    public class Entities : SqlConnect
    {
        public DataTable Users_GetAll()
        {


            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("Users_GetAll", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                //foreach (DataRow r in dt.Rows)
                //{


                //}

                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }




        }

        public DataTable Role_GetAll()
        {


            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("Role_GetAll", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }




        }

        public DataTable Result_GetAll()
        {


            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("Result_GetAll", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter(); 
                DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                //foreach (DataRow r in dt.Rows)
                //{


                //}

                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }



        
        }

        public DataTable Result_GetAll(string CCID, DateTime StartDate, DateTime EndDate, int ComMethod, int GovOrder)
        {


            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("Result_GetAll_forSearch", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("CCID", CCID);
                cmd.Parameters.AddWithValue("StartDate", StartDate);
                cmd.Parameters.AddWithValue("EndDate", EndDate);
                cmd.Parameters.AddWithValue("ComMethod", ComMethod);
                cmd.Parameters.AddWithValue("GovOrder", GovOrder);

                SqlDataAdapter adapter = new SqlDataAdapter(); 
                DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }



        
        }

        public DataTable ComMethod_GetAll()
        {


            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("CommunicationMethod_GetAll", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter(); 
                DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                //foreach (DataRow r in dt.Rows)
                //{


                //}

                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }



        
        }

        public DataTable GovOrder_GetAll()
        {


            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("GovernmentOrder_GetAll", Connection);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter(cmd); DataTable dt = new DataTable();

                adapter.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }

        }

        public DataTable ComMethod_GetAll_forDropDown()
        {


            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("CommunicationMethod_GetAll_forDropDown", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                //foreach (DataRow r in dt.Rows)
                //{


                //}

                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }




        }

        public DataTable GovOrder_GetAll_forDropDown()
        {


            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("GovernmentOrder_GetAll_forDropDown", Connection);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter(cmd); DataTable dt = new DataTable();

                adapter.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }

        }

        public void InsertResponse(Response resp)
        {

            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("InsertResult", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Date", resp.Date);
                cmd.Parameters.AddWithValue("@ComMethodID", resp.ComMethodID);
                cmd.Parameters.AddWithValue("@GovOrderID", resp.GovOrderID);
                cmd.Parameters.AddWithValue("@GovOfficialName", resp.GovOfficialName);
                cmd.Parameters.AddWithValue("@GovOfficialDept", resp.GovOfficialDept);
                cmd.Parameters.AddWithValue("@GovPosition", resp.GovPosition);
                cmd.Parameters.AddWithValue("@SubjectMatter", resp.SubjectMatter);
                cmd.Parameters.AddWithValue("@UofAMeetingParticipant", resp.UofAMeetingParticipant);
                cmd.Parameters.AddWithValue("@ExternalMeetingParticipant", resp.ExternalMeetingParticipant);
                cmd.Parameters.AddWithValue("@MeetingRequestedBy", resp.MeetingRequestedBy);
                cmd.Parameters.AddWithValue("@Comments", resp.Comments);
                cmd.Parameters.AddWithValue("@UserID", resp.UserID);
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }

        public LogOnModel GetUserRole(string UserName)
        {
            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("GetUserRole", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("UserName", UserName);
                SqlDataAdapter adapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);
                if (dt != null)
                {
                    if (dt.Rows.Count != 0)
                    {
                        LogOnModel lg = new LogOnModel();
                        lg.UserName = dt.Rows[0]["UserName"].ToString();
                        lg.Role = dt.Rows[0]["Role"].ToString();
                        lg.Status = dt.Rows[0]["Status"].ToString();
                        return lg;
                    }
                    else
                    {
                        LogOnModel lg = new LogOnModel();
                        lg.UserName = "NA";
                        lg.Role = "NA";
                        return lg;
                    }

                }
                else
                {
                    LogOnModel lg = new LogOnModel();
                    lg.UserName = "NA";
                    lg.Role = "NA";
                    return lg;
                }
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }

        public bool ExistLogin(string UserName)
        {
            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("GetUserByCCID", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("CCID", UserName);
                SqlDataAdapter adapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);


                return (dt != null);
                

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }

        public void LockUser(string CCID)
        {
            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("LockUser", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("CCID", CCID);
                SqlDataAdapter adapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }

        public User GetUserByCCID(string CCID)
        {
            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("GetUserByCCID", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CCID", CCID);
                SqlDataAdapter adapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);
                if (dt != null)
                {
                    if (dt.Rows.Count != 0)
                    {
                        User u = new User();
                        u.CCID = dt.Rows[0]["CCID"].ToString();
                        u.FirstName = dt.Rows[0]["FirstName"].ToString();
                        u.LastName = dt.Rows[0]["LastName"].ToString();
                        u.Phone = dt.Rows[0]["Phone"].ToString();
                        u.Role = Convert.ToInt16(dt.Rows[0]["RoleID"].ToString());
                        u.Status = Convert.ToInt16(dt.Rows[0]["StatusID"].ToString());
                        u.UserID = Convert.ToInt16(dt.Rows[0]["UserID"].ToString());
                        u.Title = dt.Rows[0]["Title"].ToString();


                        return u;
                    }
                    else
                    {
                        User u = new User();
                        u.CCID = "NOTFOUND";
                        return u;
                    }

                }
                else
                {
                    User u = new User();
                    u.CCID = "NOTFOUND";

                    return u;
                }
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }

        public void InsertUser(User u)
        {

            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("InsertUser", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CCID", u.CCID);
                cmd.Parameters.AddWithValue("@FirstName", n2n(u.FirstName));
                cmd.Parameters.AddWithValue("@LastName", n2n(u.LastName));
                cmd.Parameters.AddWithValue("@Phone", n2n(u.Phone));
                cmd.Parameters.AddWithValue("@Role", u.Role);
                cmd.Parameters.AddWithValue("@Status", u.Status);
                cmd.Parameters.AddWithValue("@Title", n2n(u.Title));
                cmd.Parameters.AddWithValue("@Email", n2n(u.Email));

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }

        public void DisableUser(string CCID)
        {
            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("DisableUser", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("CCID", CCID);
                SqlDataAdapter adapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                cmd.ExecuteNonQuery();
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }  
        }

        public void EnableUser(string CCID)
        {
            try
            {
                Connection.Open();

                SqlCommand cmd = new SqlCommand("EnableUser", Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("CCID", CCID);
                SqlDataAdapter adapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                cmd.ExecuteNonQuery();
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            } 
        }

        public string  n2n(string st)
        {
            if (st == null)
            {
                return "";
            }
            else return st;

        }
    }
}