USE [master]
GO

/****** Object:  Database [LRF]    Script Date: 09/12/2016 07:52:40 ******/
CREATE DATABASE [LRF] ON  PRIMARY 
( NAME = N'LRF', FILENAME = N'E:\DATA\LRF.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'LRF_log', FILENAME = N'E:\DATA\LRF_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [LRF] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LRF].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LRF] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [LRF] SET ANSI_NULLS OFF
GO
ALTER DATABASE [LRF] SET ANSI_PADDING OFF
GO
ALTER DATABASE [LRF] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [LRF] SET ARITHABORT OFF
GO
ALTER DATABASE [LRF] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [LRF] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [LRF] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [LRF] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [LRF] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [LRF] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [LRF] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [LRF] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [LRF] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [LRF] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [LRF] SET  DISABLE_BROKER
GO
ALTER DATABASE [LRF] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [LRF] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [LRF] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [LRF] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [LRF] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [LRF] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [LRF] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [LRF] SET  READ_WRITE
GO
ALTER DATABASE [LRF] SET RECOVERY FULL
GO
ALTER DATABASE [LRF] SET  MULTI_USER
GO
ALTER DATABASE [LRF] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [LRF] SET DB_CHAINING OFF
GO
USE [LRF]

GO
/****** Object:  Table [dbo].[ComMethod]    Script Date: 09/12/2016 07:52:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ComMethod](
	[ComMethodID] [int] IDENTITY(1,1) NOT NULL,
	[ComMethodName] [varchar](255) NULL,
 CONSTRAINT [PK_CommMethod] PRIMARY KEY CLUSTERED 
(
	[ComMethodID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ComMethod] ON
INSERT [dbo].[ComMethod] ([ComMethodID], [ComMethodName]) VALUES (1, N'In-person meeting')
INSERT [dbo].[ComMethod] ([ComMethodID], [ComMethodName]) VALUES (2, N'Phone call')
INSERT [dbo].[ComMethod] ([ComMethodID], [ComMethodName]) VALUES (3, N'Electronic format (e.g. email)')
INSERT [dbo].[ComMethod] ([ComMethodID], [ComMethodName]) VALUES (4, N'Hard copy (e.g. letter)')
SET IDENTITY_INSERT [dbo].[ComMethod] OFF
/****** Object:  Table [dbo].[GovOrder]    Script Date: 09/12/2016 07:52:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GovOrder](
	[GovOrderID] [int] IDENTITY(1,1) NOT NULL,
	[GovOrderName] [nvarchar](255) NULL,
 CONSTRAINT [PK_GovOrder] PRIMARY KEY CLUSTERED 
(
	[GovOrderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[GovOrder] ON
INSERT [dbo].[GovOrder] ([GovOrderID], [GovOrderName]) VALUES (1, N'Federal')
INSERT [dbo].[GovOrder] ([GovOrderID], [GovOrderName]) VALUES (2, N'Municipal')
INSERT [dbo].[GovOrder] ([GovOrderID], [GovOrderName]) VALUES (3, N'Provincial')
INSERT [dbo].[GovOrder] ([GovOrderID], [GovOrderName]) VALUES (4, N'International')
SET IDENTITY_INSERT [dbo].[GovOrder] OFF
/****** Object:  Table [dbo].[Role]    Script Date: 09/12/2016 07:52:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[ID] [int] NOT NULL,
	[name] [varchar](50) NULL,
 CONSTRAINT [PK_role] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Role] ([ID], [name]) VALUES (1, N'Administrator')
INSERT [dbo].[Role] ([ID], [name]) VALUES (2, N'User')
/****** Object:  Table [dbo].[User_status]    Script Date: 09/12/2016 07:52:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User_status](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[status] [varchar](50) NULL,
 CONSTRAINT [PK_user_status] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[User_status] ON
INSERT [dbo].[User_status] ([ID], [status]) VALUES (1, N'Active')
INSERT [dbo].[User_status] ([ID], [status]) VALUES (2, N'Disabled')
INSERT [dbo].[User_status] ([ID], [status]) VALUES (3, N'Locked')
SET IDENTITY_INSERT [dbo].[User_status] OFF
/****** Object:  Table [dbo].[User]    Script Date: 09/12/2016 07:52:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[ccid] [varchar](255) NOT NULL,
	[firstName] [varchar](50) NULL,
	[lastName] [varchar](50) NULL,
	[email] [varchar](100) NULL,
	[title] [varchar](50) NULL,
	[phone] [varchar](20) NULL,
	[statusID] [int] NULL,
	[roleID] [int] NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (1, N'koffi', N'Constant', N'Koffi', N'constant.koffi@ualberta.ca', N'Database Administrator', N'7806600434', 1, 1)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (2, N'cherene', N'Cherene', N'Griffiths', N'cherene.griffiths@ualberta.ca', N'', N'', 1, 1)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (4, N'mvicente', N'Michael', N'Vicente', N'michael.vicente@ualberta.ca', N'', N'', 1, 1)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (5, N'pdzuk', N'Patricia', N'Zuk', N'trish.zuk@ualberta.ca', N'', N'', 1, 2)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (6, N'co', N'Christina', N'Ostashevsky', N'christina.ostashevsky@ualberta.ca', N'', N'', 1, 2)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (7, N'khoshnav', N'Niloufar', N'Khoshnavaz', N'niloufar.khoshnavaz@ualberta.ca', N'', N'', 1, 2)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (8, N'callihoo', N'Nella', N'Callihoo', N'nella.sajlovic@ualberta.ca', N'', N'', 1, 2)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (9, N'webtest1', N'', N'webtest1', N'webtest1@ualberta.ca', N'', N'', 2, 2)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (10, N'hji1', N'Huiwen', N'Ji', N'huiwen.ji@ualberta.ca', N'', N'', 1, 1)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (11, N'sanabria', N'Daniel', N'Sanabria', N'sanabria@ualberta.ca', N'', N'', 1, 1)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (12, N'mmcphers', N'Margaret', N'McPherson', N'mmcphers@ualberta.ca', N'', N'', 1, 2)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (13, N'webtest2', N'', N'webtest2', N'webtest2@ualberta.ca', N'', N'', 2, 2)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (15, N'phuongfo', N'Phuong', N'Hoang', N'phuongfo@ualberta.ca', N'', N'', 1, 2)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (16, N'pagel', N'Geoffrey', N'Pagel', N'geoffrey.pagel@ualberta.ca', N'', N'', 1, 1)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (17, N'kbschmid', N'Kyle', N'Schmidt', N'kyle.schmidt@ualberta.ca', N'', N'', 1, 1)
INSERT [dbo].[User] ([UserID], [ccid], [firstName], [lastName], [email], [title], [phone], [statusID], [roleID]) VALUES (18, N'vigoren', N'Dean', N'Vigoren', N'dean.vigoren@ualberta.ca', N'', N'', 1, 1)
SET IDENTITY_INSERT [dbo].[User] OFF
/****** Object:  StoredProcedure [dbo].[Role_GetAll]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant Koffi
-- Create date: April 21 2016
-- Description:	Select All the communication Methods
-- =============================================
CREATE PROCEDURE [dbo].[Role_GetAll]

AS
BEGIN
	Set NoCount On;
	SELECT  ID
		   ,[Name]
    FROM [dbo].Role 
    where ID=1
	order by ID desc
END
GO
/****** Object:  StoredProcedure [dbo].[CommunicationMethod_GetAll_forDropDown]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant Koffi
-- Create date: April 21 2016
-- Description:	Select All the communication Methods
-- =============================================
CREATE PROCEDURE [dbo].[CommunicationMethod_GetAll_forDropDown]

AS
BEGIN
	Set NoCount On;
	SELECT 0 as ComMethodID,'All' as ComMethodName
	UNION ALL
	SELECT  [ComMethodID]
		   ,[ComMethodName]
    FROM [dbo].[ComMethod] 

END
GO
/****** Object:  StoredProcedure [dbo].[CommunicationMethod_GetAll]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant Koffi
-- Create date: April 21 2016
-- Description:	Select All the communication Methods
-- =============================================
CREATE PROCEDURE [dbo].[CommunicationMethod_GetAll]

AS
BEGIN
	Set NoCount On;
	SELECT  [ComMethodID]
		   ,[ComMethodName]
    FROM [dbo].[ComMethod] 

END
GO
/****** Object:  StoredProcedure [dbo].[GovernmentOrder_GetAll_forDropDown]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant Koffi
-- Create date: April 21 2016
-- Description:	Select All the government orders
-- =============================================
CREATE PROCEDURE [dbo].[GovernmentOrder_GetAll_forDropDown]
AS
BEGIN
	Set NoCount On;
	SELECT 0 as GovOrderID,'All' as GovOrderName
	UNION ALL
	SELECT [GovOrderID]
		  ,[GovOrderName]
	FROM   [dbo].[GovOrder]

END
GO
/****** Object:  StoredProcedure [dbo].[GovernmentOrder_GetAll]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant Koffi
-- Create date: April 21 2016
-- Description:	Select All the government orders
-- =============================================
CREATE PROCEDURE [dbo].[GovernmentOrder_GetAll]
AS
BEGIN
	Set NoCount On;
	SELECT [GovOrderID]
		  ,[GovOrderName]
	FROM   [dbo].[GovOrder]

END
GO
/****** Object:  StoredProcedure [dbo].[GetUserRole]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant Koffi
-- Create date: April 27 2016
-- Description:	Select All the government orders
-- =============================================
CREATE PROCEDURE [dbo].[GetUserRole]
	@UserName nvarchar(255)
AS
BEGIN
	Set NoCount On;
	SELECT ccid as UserName
		  ,[roleID] as Role
		  ,statusID as Status
	FROM   [dbo].[User]
	Where ccid = @UserName
END
GO
/****** Object:  StoredProcedure [dbo].[GetUserByCCID]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant Koffi
-- Create date: April 27 2016
-- Description:	Select All the government orders
-- =============================================
CREATE PROCEDURE [dbo].[GetUserByCCID]
	@CCID nvarchar(255)
AS
BEGIN
	Set NoCount On;
	SELECT [UserID],[ccid],[firstName],[lastName],[email],[title],[phone],[statusID],[roleID]
	FROM   [dbo].[User]
	Where ccid = @CCID 
END
GO
/****** Object:  StoredProcedure [dbo].[EnableUser]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant Koffi
-- Create date: May 9th 2016
-- Description:	Enable a user
-- =============================================
CREATE PROCEDURE [dbo].[EnableUser]
	@CCID nvarchar(255)
AS
BEGIN
	Set NoCount On;
	UPDATE [dbo].[User]
	SET statusID = 1
	Where ccid = @CCID 
END
GO
/****** Object:  StoredProcedure [dbo].[DisableUser]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant Koffi
-- Create date: May 9th 2016
-- Description:	disable a user
-- =============================================
CREATE PROCEDURE [dbo].[DisableUser]
	@CCID nvarchar(255)
AS
BEGIN
	Set NoCount On;
	UPDATE [dbo].[User]
	SET statusID = 2
	Where ccid = @CCID 
END
GO
/****** Object:  Table [dbo].[Responses]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Responses](
	[ResponseID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NULL,
	[ComMethodID] [int] NULL,
	[GovOrderID] [int] NULL,
	[GovOfficialName] [varbinary](max) NULL,
	[GovOfficialDept] [varbinary](max) NULL,
	[GovPosition] [varbinary](max) NULL,
	[SubjectMatter] [varbinary](max) NULL,
	[UofAMeetingParticipant] [varbinary](max) NULL,
	[ExternalMeetingParticipant] [varbinary](max) NULL,
	[MeetingRequestedBy] [varbinary](max) NULL,
	[Comments] [varbinary](max) NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_Responses] PRIMARY KEY CLUSTERED 
(
	[ResponseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[LockUser]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant Koffi
-- Create date: May 9th 2016
-- Description:	disable a user
-- =============================================
CREATE PROCEDURE [dbo].[LockUser]
	@CCID nvarchar(255)
AS
BEGIN
	Set NoCount On;
	UPDATE [dbo].[User]
	SET statusID = 3
	Where ccid = @CCID 
END
GO
/****** Object:  StoredProcedure [dbo].[InsertUser]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant koffi
-- Create date: April 22 2016
-- Description:	Insert a new user
-- =============================================
CREATE PROCEDURE [dbo].[InsertUser] 

			@CCID nvarchar(255),
            @FirstName nvarchar(255),
            @LastName nvarchar(255),
            @Phone nvarchar(255),
            @Role int,
            @Status int,
            @Title nvarchar(255),
            @Email nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO [dbo].[User]
           (CCID
           ,FirstName
           ,LastName
           ,Phone
           ,RoleID
           ,StatusID
           ,Title
           ,Email)
VALUES
           (@CCID
           ,@FirstName
           ,@LastName
           ,@Phone
           ,@Role
           ,@Status
           ,@Title
           ,@Email
		   )
		   

END
GO
/****** Object:  StoredProcedure [dbo].[Users_GetAll]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant koffi
-- Create date: April 22 2016
-- Description:	Insert Encrypted Lobbying survey response
-- =============================================
CREATE PROCEDURE [dbo].[Users_GetAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT      
			u.[UserID],
			u.[ccid],
			u.[firstName],
			u.[lastName],
			u.[email],
			u.[title],
			r.name as Role,
			s.status as status
	FROM [dbo].[user] u
	inner join [dbo].[User_status] s on s.id = u.statusID
	inner join  [dbo].[role] r on r.ID = u.roleID
	order by u.firstname, u.lastname desc;



END
GO
/****** Object:  StoredProcedure [dbo].[Result_GetAll_forSearch]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant koffi
-- Create date: April 22 2016
-- Description:	Insert Encrypted Lobbying survey response
-- =============================================
CREATE PROCEDURE [dbo].[Result_GetAll_forSearch]
@CCID nvarchar(255),
@StartDate Datetime,
@EndDate Datetime,
@ComMethod int,
@GovOrder int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY LRF_Key01
    DECRYPTION BY CERTIFICATE UDSEncryptCertif;
	IF @ComMethod=0 and @GovOrder=0 
	BEGIN
			SELECT   
	   
						CONVERT(VARCHAR(11),[Date],101) as [Date]
					   ,r.[ComMethodID]
					   ,m.ComMethodName
					   ,r.[GovOrderID]
					   ,g.GovOrderName
					   ,CONVERT(nvarchar(255), DecryptByKey(GovOfficialName)) as GovOfficialName
					   ,CONVERT(nvarchar(255), DecryptByKey(GovOfficialDept)) as GovOfficialDept
					   ,CONVERT(nvarchar(255), DecryptByKey(GovPosition)) as GovPosition
					   ,CONVERT(nvarchar(255), DecryptByKey(SubjectMatter)) as SubjectMatter
					   ,CONVERT(nvarchar(255), DecryptByKey(UofAMeetingParticipant)) as UofAMeetingParticipant
					   ,CONVERT(nvarchar(255), DecryptByKey(ExternalMeetingParticipant)) as ExternalMeetingParticipant
					   ,CONVERT(nvarchar(255), DecryptByKey(MeetingRequestedBy)) as MeetingRequestedBy
					   ,CONVERT(nvarchar(max), DecryptByKey(Comments)) as Comments
					   ,u.firstName
					   ,u.lastName
					   ,u.ccid
					   ,u.firstName + ' ' + u.lastName as FullName
			FROM [dbo].[Responses] r 
			inner join [dbo].[GovOrder] g on g.GovOrderID = r.GovOrderID
			inner join  [dbo].[ComMethod] m on m.ComMethodID = r.ComMethodID
			inner join [dbo].[User] u on u.UserID = r.UserID
			where u.ccid + u.firstName + u.lastName like '%'+ @CCID + '%' and r.[Date] between @StartDate and @EndDate
			order by date desc;
	END
	ELSE IF @ComMethod=0 
	BEGIN
			SELECT   
	   
						CONVERT(VARCHAR(11),[Date],101) as [Date]
					   ,r.[ComMethodID]
					   ,m.ComMethodName
					   ,r.[GovOrderID]
					   ,g.GovOrderName
					   ,CONVERT(nvarchar(255), DecryptByKey(GovOfficialName)) as GovOfficialName
					   ,CONVERT(nvarchar(255), DecryptByKey(GovOfficialDept)) as GovOfficialDept
					   ,CONVERT(nvarchar(255), DecryptByKey(GovPosition)) as GovPosition
					   ,CONVERT(nvarchar(255), DecryptByKey(SubjectMatter)) as SubjectMatter
					   ,CONVERT(nvarchar(255), DecryptByKey(UofAMeetingParticipant)) as UofAMeetingParticipant
					   ,CONVERT(nvarchar(255), DecryptByKey(ExternalMeetingParticipant)) as ExternalMeetingParticipant
					   ,CONVERT(nvarchar(255), DecryptByKey(MeetingRequestedBy)) as MeetingRequestedBy
					   ,CONVERT(nvarchar(max), DecryptByKey(Comments)) as Comments
					   ,u.firstName
					   ,u.lastName
					   ,u.ccid
					   ,u.firstName + ' ' + u.lastName as FullName
			FROM [dbo].[Responses] r 
			inner join [dbo].[GovOrder] g on g.GovOrderID = r.GovOrderID
			inner join  [dbo].[ComMethod] m on m.ComMethodID = r.ComMethodID
			inner join [dbo].[User] u on u.UserID = r.UserID
			where u.ccid + u.firstName + u.lastName like '%'+ @CCID + '%' and r.[Date] between @StartDate and @EndDate and r.GovOrderID = @GovOrder
			order by date desc;
		END
		ELSE
		BEGIN
			SELECT   
	   
						CONVERT(VARCHAR(11),[Date],101) as [Date]
					   ,r.[ComMethodID]
					   ,m.ComMethodName
					   ,r.[GovOrderID]
					   ,g.GovOrderName
					   ,CONVERT(nvarchar(255), DecryptByKey(GovOfficialName)) as GovOfficialName
					   ,CONVERT(nvarchar(255), DecryptByKey(GovOfficialDept)) as GovOfficialDept
					   ,CONVERT(nvarchar(255), DecryptByKey(GovPosition)) as GovPosition
					   ,CONVERT(nvarchar(255), DecryptByKey(SubjectMatter)) as SubjectMatter
					   ,CONVERT(nvarchar(255), DecryptByKey(UofAMeetingParticipant)) as UofAMeetingParticipant
					   ,CONVERT(nvarchar(255), DecryptByKey(ExternalMeetingParticipant)) as ExternalMeetingParticipant
					   ,CONVERT(nvarchar(255), DecryptByKey(MeetingRequestedBy)) as MeetingRequestedBy
					   ,CONVERT(nvarchar(max), DecryptByKey(Comments)) as Comments
					   ,u.firstName
					   ,u.lastName
					   ,u.ccid
					   ,u.firstName + ' ' + u.lastName as FullName
			FROM [dbo].[Responses] r 
			inner join [dbo].[GovOrder] g on g.GovOrderID = r.GovOrderID
			inner join  [dbo].[ComMethod] m on m.ComMethodID = r.ComMethodID
			inner join [dbo].[User] u on u.UserID = r.UserID
			where u.ccid + u.firstName + u.lastName like '%'+ @CCID + '%' and r.[Date] between @StartDate and @EndDate and r.ComMethodID = @ComMethod
			order by date desc;
		END


END
GO
/****** Object:  StoredProcedure [dbo].[Result_GetAll]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant koffi
-- Create date: April 22 2016
-- Description:	Insert Encrypted Lobbying survey response
-- =============================================
CREATE PROCEDURE [dbo].[Result_GetAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY LRF_Key01
    DECRYPTION BY CERTIFICATE UDSEncryptCertif;

	SELECT      
				[Date]
			   ,r.[ComMethodID]
			   ,m.ComMethodName
			   ,r.[GovOrderID]
			   ,g.GovOrderName
			   ,CONVERT(nvarchar(255), DecryptByKey(GovOfficialName)) as GovOfficialName
			   ,CONVERT(nvarchar(255), DecryptByKey(GovOfficialDept)) as GovOfficialDept
			   ,CONVERT(nvarchar(255), DecryptByKey(GovPosition)) as GovPosition
			   ,CONVERT(nvarchar(255), DecryptByKey(SubjectMatter)) as SubjectMatter
			   ,CONVERT(nvarchar(255), DecryptByKey(UofAMeetingParticipant)) as UofAMeetingParticipant
			   ,CONVERT(nvarchar(255), DecryptByKey(ExternalMeetingParticipant)) as ExternalMeetingParticipant
			   ,CONVERT(nvarchar(255), DecryptByKey(MeetingRequestedBy)) as MeetingRequestedBy
			   ,CONVERT(nvarchar(max), DecryptByKey(Comments)) as Comments
			   ,u.firstName
			   ,u.lastName
			   ,u.ccid
			   ,u.firstName + ' ' + u.lastName as FullName
	FROM [dbo].[Responses] r 
	inner join [dbo].[GovOrder] g on g.GovOrderID = r.GovOrderID
	inner join  [dbo].[ComMethod] m on m.ComMethodID = r.ComMethodID
	inner join [dbo].[User] u on u.UserID = r.UserID
	order by date desc;



END
GO
/****** Object:  StoredProcedure [dbo].[InsertResult]    Script Date: 09/12/2016 07:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Constant koffi
-- Create date: April 22 2016
-- Description:	Insert Encrypted Lobbying survey response
-- =============================================
CREATE PROCEDURE [dbo].[InsertResult] 

			@Date datetime,
            @ComMethodID int,
            @GovOrderID int,
            @GovOfficialName nvarchar(255),
            @GovOfficialDept  nvarchar(255),
            @GovPosition  nvarchar(255),
            @SubjectMatter  nvarchar(255),
            @UofAMeetingParticipant  nvarchar(255),
            @ExternalMeetingParticipant  nvarchar(255),
            @MeetingRequestedBy  nvarchar(255),
            @Comments  nvarchar(max),
			@UserID nvarchar (255)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY LRF_Key01
    DECRYPTION BY CERTIFICATE UDSEncryptCertif;

INSERT INTO [dbo].[Responses]
           ([Date]
           ,[ComMethodID]
           ,[GovOrderID]
           ,[GovOfficialName]
           ,[GovOfficialDept]
           ,[GovPosition]
           ,[SubjectMatter]
           ,[UofAMeetingParticipant]
           ,[ExternalMeetingParticipant]
           ,[MeetingRequestedBy]
           ,[Comments]
		   ,UserID)
VALUES
           (@Date
           ,@ComMethodID
           ,@GovOrderID
           ,EncryptByKey(Key_GUID('LRF_Key01'), @GovOfficialName)
		   ,EncryptByKey(Key_GUID('LRF_Key01'), @GovOfficialDept)
		   ,EncryptByKey(Key_GUID('LRF_Key01'), @GovPosition)
		   ,EncryptByKey(Key_GUID('LRF_Key01'), @SubjectMatter)
           ,EncryptByKey(Key_GUID('LRF_Key01'), @UofAMeetingParticipant)
           ,EncryptByKey(Key_GUID('LRF_Key01'), @ExternalMeetingParticipant)
		   ,EncryptByKey(Key_GUID('LRF_Key01'), @MeetingRequestedBy)
		   ,EncryptByKey(Key_GUID('LRF_Key01'), @Comments)
		   ,@UserID
		   )
		   

END
GO
/****** Object:  ForeignKey [FK_User_Role]    Script Date: 09/12/2016 07:52:42 ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([roleID])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Role]
GO
/****** Object:  ForeignKey [FK_User_user_status]    Script Date: 09/12/2016 07:52:42 ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_user_status] FOREIGN KEY([statusID])
REFERENCES [dbo].[User_status] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_user_status]
GO
/****** Object:  ForeignKey [FK_Responses_ComMethod]    Script Date: 09/12/2016 07:52:44 ******/
ALTER TABLE [dbo].[Responses]  WITH CHECK ADD  CONSTRAINT [FK_Responses_ComMethod] FOREIGN KEY([ComMethodID])
REFERENCES [dbo].[ComMethod] ([ComMethodID])
GO
ALTER TABLE [dbo].[Responses] CHECK CONSTRAINT [FK_Responses_ComMethod]
GO
/****** Object:  ForeignKey [FK_Responses_GovOrder]    Script Date: 09/12/2016 07:52:44 ******/
ALTER TABLE [dbo].[Responses]  WITH CHECK ADD  CONSTRAINT [FK_Responses_GovOrder] FOREIGN KEY([GovOrderID])
REFERENCES [dbo].[GovOrder] ([GovOrderID])
GO
ALTER TABLE [dbo].[Responses] CHECK CONSTRAINT [FK_Responses_GovOrder]
GO
/****** Object:  ForeignKey [FK_Responses_User]    Script Date: 09/12/2016 07:52:44 ******/
ALTER TABLE [dbo].[Responses]  WITH CHECK ADD  CONSTRAINT [FK_Responses_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Responses] CHECK CONSTRAINT [FK_Responses_User]
GO
