﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Routing;
using System.Web.Security;
using System.Security.Principal;
using System.Threading;
using System.DirectoryServices.Protocols;
using System.DirectoryServices;
using System.Net;
using LobbyingReportForm.STRUCTURES;
using LobbyingReportForm.DBACCESS;
using System.Text.RegularExpressions;
namespace LobbyingReportForm
{
    public partial class Login : System.Web.UI.Page
    {
       #region LDAP Variables


        private const int _LdapPort = 389;
        private const string _LdapHost = "directory.srv.ualberta.ca";
        private const string _LdapSearchBase = "ou=people,dc=ualberta,dc=ca";
        private static string[] _LdapAttributes = new string[] { "uid", "cn", "sn", "mail", "title", "telephonenumber", "givenname","department" };
        private static LdapConnection _LdapConn = null;
        private const int _LdapSecurePort = 636;
        private static bool BindResult { get; set; }
        private static string UserPassword { get; set; }
        private static string UserDn { get; set; }
        private static string ErrorMessage { get; set; }


        #endregion

        #region Ldap Connection

        /// <summary>
        /// Creates a connection to the Ldap service
        /// </summary>
        /// <returns>True if connection was made or exists, False if connection was unable to be made</returns>
        public static bool LdapConnect()
        {
            bool result = true;
            if (_LdapConn == null)
            {
                try
                {
                    _LdapConn = new LdapConnection(new LdapDirectoryIdentifier(_LdapHost, _LdapPort));
                    _LdapConn.AuthType = AuthType.Basic;
                    _LdapConn.Bind();
                }
                catch (Exception e)
                {
                    result = false;
                    throw (e);
                }
            }
            return result;
        }

        /// <summary>
        /// Disconnects the current Ldap Connection
        /// </summary>
        /// <returns>True if the connection was disconnected, False if the connection was unable to be disconnected.</returns>
        public static bool LdapDisconnect()
        {
            bool result = true;
            if (_LdapConn != null)
            {
                try
                {
                    _LdapConn.Dispose();
                    _LdapConn = null;
                }
                catch (Exception e)
                {
                    result = false;
                    throw (e);
                }
            }
            return result;
        }

        #endregion

        // POST: /Account/LogOn
        private void DoAuthentication()
        {
            BindResult = false;

            try
            {
                var ldapConn = new LdapConnection(new LdapDirectoryIdentifier(_LdapHost, _LdapSecurePort), new System.Net.NetworkCredential(UserDn, UserPassword), AuthType.Basic);
                ldapConn.SessionOptions.SecureSocketLayer = true;
                ldapConn.Bind();
                ldapConn.Dispose();

                BindResult = true;
            }
            catch (LdapException x)
            {
                throw (x);
            }
            catch (Exception x)
            {
                throw (x);
            }
        }

        public static SearchResultEntry GetLdapUser(string ccid)
        {
            SearchResultEntry data = null;
            try
            {
                bool createConn = false;
                if (_LdapConn == null)
                {
                    if (LdapConnect())
                    {
                        createConn = true;
                    }
                    else
                    {
                        throw new Exception("LdapConnect Error: Unable to created connection to Ldap.");
                    }
                }
                SearchRequest ldapSearchReq = new SearchRequest(_LdapSearchBase, string.Format("uid={0}", ccid), SearchScope.OneLevel, _LdapAttributes);
                SearchResponse ldapSearchResp = (SearchResponse)_LdapConn.SendRequest(ldapSearchReq);
                if (ldapSearchResp.Entries.Count > 0)
                {
                   
                    data = ldapSearchResp.Entries[0];
                }
                if (createConn)
                {
                    LdapDisconnect();
                }
            }
            catch (Exception e)
            {
                throw(e);
            }
            return data;
        }

        protected void BtLogin_Click(object sender, EventArgs e)
        {
            if (TbUserName.Value == "" || TbPassword.Value == "")
            {
                LblError.Text = "Please enter login and password.";
            }
            else if (!isAlphaNumeric(TbUserName.Value))
            {
                LblError.Text = "Please enter a valid CCID.";
            }
            else
            { 
                        LogOnModel UserRole = (new Entities()).GetUserRole(TbUserName.Value);
                        if (UserRole.Status == "2")
                        {
                             LblError.Text = "User disabled.Please contact the administrator";
                        }
                        else if (UserRole.Status == "3")
                        {
                             LblError.Text = "User locked.Please contact the administrator";
                        }
                        else if (ValidateUser(TbUserName.Value, TbPassword.Value))
                            {

                                FormsAuthentication.SetAuthCookie(TbUserName.Value, UserRole.RememberMe);

                                    if (UserRole.UserName == "NA")
                                    {
                                        //find ccid and add it

                                        User theUser = (new Entities()).GetUserByCCID(TbUserName.Value);

                                        if (theUser.CCID == "NOTFOUND")
                                        {
                                            //insert user

                                            SearchResultEntry data = Login.GetLdapUser(TbUserName.Value);

                                            if (data == null)
                                            {
                                                LblError.Text = "User not found in LDAP. Please contact administrator.";
                                            }
                                            else
                                            {

                                                User NewUser = new User();

                                                if (data.Attributes.Contains("givenname"))
                                                {
                                                    NewUser.FirstName = GetEntryValue(data, "givenname");
                                                }
                                                if (data.Attributes.Contains("sn"))
                                                {
                                                    NewUser.LastName = GetEntryValue(data, "sn");
                                                }
                                                if (data.Attributes.Contains("mail"))
                                                {
                                                    NewUser.Email = GetEntryValue(data, "mail");
                                                }
                                                if (data.Attributes.Contains("title"))
                                                {
                                                    NewUser.Title = GetEntryValue(data, "title");
                                                }
                                                if (data.Attributes.Contains("telephonenumber"))
                                                {
                                                    NewUser.Phone = GetEntryValue(data, "telephonenumber");
                                                }

                                                NewUser.Status = 1;
                                                NewUser.CCID = TbUserName.Value.ToLower();
                                                NewUser.Role = 2;

                                                (new Entities()).InsertUser(NewUser);
                                            }

                                        }

                                        Response.Redirect("LobbyingReportForm.aspx");
                                    }
                                    else
                                    { 
                                        if (Role.Value == "2")
                                        { 
                                            Response.Redirect("LobbyingReportForm.aspx");
                                        }
                                        else
                                        {
                                            if(UserRole.Role=="1")
                                            {
                                                Response.Redirect("AdminDownload.aspx");
                                            }
                                            else
                                            {
                                                LblError.Text = "Access Denied. Admin role required!";
                                            }
                                        }
                                    }
                            }
                            else
                            {
                                LblError.Text = "Access Denied. LDAP validation failed!";
                            }
            }
        }

        private static String GetEntryValue(SearchResultEntry entry, String attribute)
        {
            return String.Join(",", entry.Attributes[attribute].GetValues(attribute.GetType()));
        }

        public bool ValidateUser(string username, string password)
        {

            if (Session["loginCount"] == null) //setup the session var with 0 count
            {
                Session.Add("loginCount", 0);
            }


            try{

                SearchResultEntry UserData = GetLdapUser(username);

                if (UserData != null)
                {
                    UserDn = UserData.DistinguishedName;
                    UserPassword = password;
                    DoAuthentication();
                }



            }
            catch (ThreadAbortException)
            {
                // Do nothing - user was redirected in Authentication.cs
                return false;
            }
            catch (Exception x )
            {
                 LblError.Text = "The supplied credential is invalid.";
                //throw (x); 

                 Session["loginCount"] = (int)Session["loginCount"] + 1;


                 if ((int)Session["loginCount"] >= 4)
                 {

                     if ((new Entities()).ExistLogin(TbUserName.Value)) { (new Entities()).LockUser(TbUserName.Value); LblError.Text = "Too many failed attempts. User account locked."; }
                     
                 }

                return false;
            }

            return true;

        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public static Boolean isAlphaNumeric(string strToCheck)
        {
            Regex rg = new Regex(@"^[a-zA-Z0-9\s,]*$");
            return rg.IsMatch(strToCheck);
        }
}  
}