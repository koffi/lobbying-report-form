﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Submitted.aspx.cs" Inherits="LobbyingReportForm.Submitted" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js"></script> 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
 <link href="css/Site.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            font-size: medium;
            text-align: justify;
        }
        input[type="text"] {
            height: 2em;
        }
    </style>
    <link href="https://www.ualberta.ca/~publicas/uofa/css/homepage-fluid.css" rel="stylesheet">
    <link href="https://uofa.ualberta.ca/design/css/framework.css" rel="stylesheet">

</head>
<body>
    <header> 
    <div class="blade-container ga_ualberta_blade" style="height:inherit;"><!-- Start Blade -->
      <div class="blade-inner">
        <a class="brand" href="#"></a>
        <nav class="blade-navigation">
          <ul class="quick-links-container">
            <li id="ql" class="dropdown">
              <a href="#" id="ql-toggle" class="btn btn-grey btn-small hidden-desktop">
                <span class="quick-links-text">
                  Quick Links <b class="caret"></b>
                </span>
                <span class="icon-bar quick-links-button"></span>
                <span class="icon-bar quick-links-button"></span>
                <span class="icon-bar quick-links-button"></span>
              </a>
              <ul class="quick-links" role="menu" aria-labelledby="ql-toggle">
              <li role="menuitem"><a href="http://myualberta.ualberta.ca">My UAlberta</a></li>
                <li role="menuitem"><a href="http://webapps.srv.ualberta.ca/search/">Find a Person</a></li>
                <li role="menuitem"><a href="http://www.onecard.ualberta.ca/">ONEcard</a></li>
                <li role="menuitem"><a href="https://www.beartracks.ualberta.ca/">Bear Tracks</a></li>
                <li role="menuitem"><a href="http://www.campusmap.ualberta.ca/">Maps</a></li>
                <li role="menuitem"><a href="http://apps.ualberta.ca/">Email &amp; Apps</a></li>
                <li role="menuitem"><a href="https://eclass.srv.ualberta.ca/portal/">eClass</a></li>
                <li role="menuitem"><a href="http://www.library.ualberta.ca/">Libraries</a></li>
              </ul>
            </li>
          </ul>
        </nav>
        <div class="blade-search">
           <i class="icon-search"></i>
           <input type="search" class="search-query" placeholder="search">
           <button type="submit" class="btn btn-grey btn-small global-search-btn">Search</button>
        </div>
      </div>
    </div> <!-- End Blade -->
    <div class="top-header row-fluid">
      <div class="banner-inner">
        <h1 class="hp-h1">
          <span class="identifier"></span>
          <span class="site-title">University of Alberta</span>
        </h1>
        <div class="banner-logo" style="display: block;">
          <a href="http://www.ualberta.ca">
            <img src="http://www.ualberta.ca/~publicas/uofa/img/logo-reverse.svg" class="site-logo" alt="University of Alberta" />
          </a>
          </div>

            <nav class="audience-navigation" style="right: 20%;">
                    <ul>
                    <li role="menuitem" class="audience-link">Welcome <asp:Label ID="LbUserName" runat="server"></asp:Label>![ <a href="/Login.aspx">Logout</a> ]</li>
                    </ul>
            </nav>
    </div>
    </div>

</header> <!-- End Header -->


<div class="content-wrapper">
  <div class="content-container container">
    <div class="page-title-row">
        <div class="title-container">
            <span class="section-heading">Government and Stakeholder Relations</span>
            <div align="right" id="logindisplay">
            </div>
        </div>
    </div>
    <div>
        <h3>MONTHLY LOBBYING REPORT FORM</h3>
     </div>


            <div class="content-container">
            <div class="form-area">
                    <form id="form1" runat="server">
                    <div class="form-group" style="height:400px;vertical-align:middle;text-align:center;font-size:larger;">
                     <p>Thanks! Your response has been successfully submitted.</p>
                    <div>
                                <asp:Button ID="ButtonNext" runat="server" class="btn btn-success" Text="Submit an additional record" OnClick="ButtonNext_Click" />
                                &nbsp;&nbsp;
                                <asp:Button ID="ButtonLogout" runat="server" class="btn btn-success" Text="Close" OnClick="ButtonLogout_Click" />
                            &nbsp;
                    </div>

                    </div>                            

                    </form>
                </div>
     </div>
</div>
       </div>
            </div>
    <div class="bottom-footer-row ga_ualberta_footer"> <!-- Footer Bottom Row Start -->
      <div class="footer-container container"> <!-- Footer Container Start -->
        <div class="footer-inner row-fluid"> <!-- Footer Inner Start -->
        
          <div class="span12 footer-copyright">
            <ul class="footer-quick-links">
              <li><a href="http://uofa.ualberta.ca/contactualberta">Contact Us</a></li>
              <li><a href="https://www.surveymonkey.com/s/ualberta-web-feedback">Feedback</a></li>
              <li><a href="http://calendar.ualberta.ca">University Calendar</a></li>
              <li><a href="http://www.careers.ualberta.ca/">Careers</a></li>
              <li><a href="http://www.rms.ualberta.ca/en/EmergNumbers.aspx">Emergency</a></li>
              <li><a href="https://policiesonline.ualberta.ca/PoliciesProcedures/Pages/default.aspx">Policies</a></li>
              <li><a href="http://weather.gc.ca/city/pages/ab-50_metric_e.html" target="_blank">Weather</a></li>
              <li><a href="http://www.edmonton.ca/" target="_blank">Edmonton</a></li>
              <li><a href="http://uofa.ualberta.ca/privacy">Privacy</a></li>
              <li><a href="http://uofa.ualberta.ca/keepintouch">Keep in Touch</a></li>
              <li><a href="#">Back to Top</a></li>
            </ul>
            <p>
              &copy; <script>document.write(new Date().getFullYear())</script> University of Alberta 116 St. and 85 Ave., <br class="visible-phone" />Edmonton, AB, Canada T6G 2R3
            </p>
          </div>
        </div> <!-- Footer Inner End -->
      </div>  <!-- Footer Container End -->
    </div> <!-- Footer Bottom Row End -->
   <!-- Footer End -->
</body>
</html>
