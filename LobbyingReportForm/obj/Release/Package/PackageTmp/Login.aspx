﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="LobbyingReportForm.Login" %>

<!DOCTYPE html>


<html>
<head>
        <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
    <title>Monthly Lobbying Report Form</title>

    <link href="css/Site.css" rel="stylesheet" />

    <link href="https://www.ualberta.ca/~publicas/uofa/css/homepage-fluid.css" rel="stylesheet">
    <link href="https://uofa.ualberta.ca/design/css/framework.css" rel="stylesheet">    
    <style type="text/css">
        .auto-style1 {
            font-size: medium;
            text-align: justify;
        }
        input {
            height: 2em;
        }
    </style>
</head>
<body>
    <header> 
 <div class="blade-container ga_ualberta_blade" style="height:inherit;"><!-- Start Blade -->
      <div class="blade-inner">
        <a class="brand" href="#"></a>
        <nav class="blade-navigation">
          <ul class="quick-links-container">
            <li id="ql" class="dropdown">
              <a href="#" id="ql-toggle" class="btn btn-grey btn-small hidden-desktop">
                <span class="quick-links-text">
                  Quick Links <b class="caret"></b>
                </span>
                <span class="icon-bar quick-links-button"></span>
                <span class="icon-bar quick-links-button"></span>
                <span class="icon-bar quick-links-button"></span>
              </a>
              <ul class="quick-links" role="menu" aria-labelledby="ql-toggle">
              <li role="menuitem"><a href="http://myualberta.ualberta.ca">My UAlberta</a></li>
                <li role="menuitem"><a href="http://webapps.srv.ualberta.ca/search/">Find a Person</a></li>
                <li role="menuitem"><a href="http://www.onecard.ualberta.ca/">ONEcard</a></li>
                <li role="menuitem"><a href="https://www.beartracks.ualberta.ca/">Bear Tracks</a></li>
                <li role="menuitem"><a href="http://www.campusmap.ualberta.ca/">Maps</a></li>
                <li role="menuitem"><a href="http://apps.ualberta.ca/">Email &amp; Apps</a></li>
                <li role="menuitem"><a href="https://eclass.srv.ualberta.ca/portal/">eClass</a></li>
                <li role="menuitem"><a href="http://www.library.ualberta.ca/">Libraries</a></li>
              </ul>
            </li>
          </ul>
        </nav>
        <div class="blade-search">
           <i class="icon-search"></i>
           <input type="search" class="search-query" placeholder="search">
           <button type="submit" class="btn btn-grey btn-small global-search-btn">Search</button>
        </div>
      </div>
    </div> <!-- End Blade -->
    <div class="top-header row-fluid">
      <div class="banner-inner">
        <h1 class="hp-h1">
          <span class="identifier"></span>
          <span class="site-title">University of Alberta</span>
        </h1>
        <div class="banner-logo" style="display: block;">
          <a href="http://www.ualberta.ca">
            <img src="http://www.ualberta.ca/~publicas/uofa/img/logo-reverse.svg" class="site-logo" alt="University of Alberta" />
          </a>
    </div>
  </div>
</div>
</header> <!-- End Header -->

<div class="content-wrapper">
  <div class="content-container container">
    <div class="page-title-row">
        <div class="title-container">
            <span class="section-heading">Government and Stakeholder Relations</span>
            <div align="right" id="logindisplay">
            </div>
        </div>
    </div>
    <div>
        <h3>MONTHLY LOBBYING REPORT FORM</h3>

<div class="content-container">
            
<script type="text/javascript">
      $(document).ajaxError(function (xhr, props) {
          xhr.stopPropagation();
          if (props.status === 401) {
              window.alert("Your session has timed out!");
              location.reload();
          }
      });
</script>

<p>
    Please log in with your University of Alberta CCID and password.
</p>

<script src="/Scripts/jquery.validate.min.js" type="text/javascript"></script>

<form runat="server" method="post">    
    <div>

            <div class="editor-label">
                <label for="UserName">CCID</label>
            </div>

            <div class="editor-field" >
                <input runat="server" data-val="true" style="height:2em" data-val-required="The CCID field is required." id="TbUserName" name="LblUserName" type="text" value=""  placeholder="Please enter your CCID" />
                <span class="field-validation-valid" data-valmsg-for="UserName" data-valmsg-replace="true"></span>
            </div>

            <div class="editor-label">
                <label for="Password">Password</label>
            </div>
            <div class="editor-field" >
                <input runat="server" data-val="true" style="height:2em" data-val-required="The Password field is required." id="TbPassword" name="LblPassword" type="password"  placeholder="Please enter your Password"/>
                <span class="field-validation-valid" data-valmsg-for="Password" data-valmsg-replace="true"></span>
            </div>
            <div class="editor-label">
                <label for="Role">Role</label>
            </div>
            <div class="editor-field">
                <select runat="server" id="Role" name="Role"><option value="2">Lobbyist</option>
                    <option value="1">Administrator</option>
                </select>
                <span class="field-validation-valid" data-valmsg-for="Role" data-valmsg-replace="true"></span>
            </div>
            <p>
                <asp:Button ID="ButtonLogin" runat="server" Text="Submit" OnClick="BtLogin_Click" />
            </p>

            <asp:Label ID="LblError" runat="server" style="color: #FF0000"></asp:Label> 
    </div>
</form>
        <br />
        </div>
    </div>
  </div>
 <div class="bottom-footer-row ga_ualberta_footer"> <!-- Footer Bottom Row Start -->
      <div class="footer-container container"> <!-- Footer Container Start -->
        <div class="footer-inner row-fluid"> <!-- Footer Inner Start -->
        
          <div class="span12 footer-copyright">
            <ul class="footer-quick-links">
              <li><a href="http://uofa.ualberta.ca/contactualberta">Contact Us</a></li>
              <li><a href="https://www.surveymonkey.com/s/ualberta-web-feedback">Feedback</a></li>
              <li><a href="http://calendar.ualberta.ca">University Calendar</a></li>
              <li><a href="http://www.careers.ualberta.ca/">Careers</a></li>
              <li><a href="http://www.rms.ualberta.ca/en/EmergNumbers.aspx">Emergency</a></li>
              <li><a href="https://policiesonline.ualberta.ca/PoliciesProcedures/Pages/default.aspx">Policies</a></li>
              <li><a href="http://weather.gc.ca/city/pages/ab-50_metric_e.html" target="_blank">Weather</a></li>
              <li><a href="http://www.edmonton.ca/" target="_blank">Edmonton</a></li>
              <li><a href="http://uofa.ualberta.ca/privacy">Privacy</a></li>
              <li><a href="http://uofa.ualberta.ca/keepintouch">Keep in Touch</a></li>
              <li><a href="#">Back to Top</a></li>
            </ul>
            <p>
              &copy; <script>document.write(new Date().getFullYear())</script> University of Alberta 116 St. and 85 Ave., <br class="visible-phone" />Edmonton, AB, Canada T6G 2R3
            </p>
          </div>
        </div> <!-- Footer Inner End -->
      </div>  <!-- Footer Container End -->
    </div> <!-- Footer Bottom Row End -->
   <!-- Footer End -->
    </div>
</body>
</html>
