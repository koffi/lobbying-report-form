﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminDownload.aspx.cs" Inherits="LobbyingReportForm.Admin" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Monthly Lobbying Report Form</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js"></script> 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
      $(function () {
          var StartDate = new Date();
          StartDate.setMonth(StartDate.getMonth() - 1);
          $("#TbStartDate").datepicker();
          $("#TbStartDate").datepicker('setDate', StartDate);
          $("#TbEndDate").datepicker();
          $("#TbEndDate").datepicker('setDate', new Date());
    });
  </script>

    <link href="css/Site.css" rel="stylesheet" />
    <style type="text/css">
        input[type="text"] {
            height: 2em;
        }
    </style>
    <link href="https://www.ualberta.ca/~publicas/uofa/css/homepage-fluid.css" rel="stylesheet">
    <link href="https://uofa.ualberta.ca/design/css/framework.css" rel="stylesheet">
</head>
<body>
<header> 
 <div class="blade-container ga_ualberta_blade" style="height:inherit;"><!-- Start Blade -->
      <div class="blade-inner">
        <a class="brand" href="#"></a>
        <nav class="blade-navigation">
          <ul class="quick-links-container">
            <li id="ql" class="dropdown">
              <a href="#" id="ql-toggle" class="btn btn-grey btn-small hidden-desktop">
                <span class="quick-links-text">
                  Quick Links <b class="caret"></b>
                </span>
                <span class="icon-bar quick-links-button"></span>
                <span class="icon-bar quick-links-button"></span>
                <span class="icon-bar quick-links-button"></span>
              </a>
              <ul class="quick-links" role="menu" aria-labelledby="ql-toggle">
              <li role="menuitem"><a href="http://myualberta.ualberta.ca">My UAlberta</a></li>
                <li role="menuitem"><a href="http://webapps.srv.ualberta.ca/search/">Find a Person</a></li>
                <li role="menuitem"><a href="http://www.onecard.ualberta.ca/">ONEcard</a></li>
                <li role="menuitem"><a href="https://www.beartracks.ualberta.ca/">Bear Tracks</a></li>
                <li role="menuitem"><a href="http://www.campusmap.ualberta.ca/">Maps</a></li>
                <li role="menuitem"><a href="http://apps.ualberta.ca/">Email &amp; Apps</a></li>
                <li role="menuitem"><a href="https://eclass.srv.ualberta.ca/portal/">eClass</a></li>
                <li role="menuitem"><a href="http://www.library.ualberta.ca/">Libraries</a></li>
              </ul>
            </li>
          </ul>
        </nav>
        <div class="blade-search">
           <i class="icon-search"></i>
           <input type="search" class="search-query" placeholder="search">
           <button type="submit" class="btn btn-grey btn-small global-search-btn">Search</button>
        </div>
      </div>
    </div> <!-- End Blade -->
    <div class="top-header row-fluid">
      <div class="banner-inner">
        <h1 class="hp-h1">
          <span class="identifier"></span>
          <span class="site-title">University of Alberta</span>
        </h1>
        <div class="banner-logo" style="display: block;">
          <a href="http://www.ualberta.ca">
            <img src="http://www.ualberta.ca/~publicas/uofa/img/logo-reverse.svg" class="site-logo" alt="University of Alberta" />
          </a>
          </div>
            <nav class="audience-navigation" style="right: 20%;">
                    <ul>
                    <li role="menuitem" class="audience-link">Welcome <asp:Label ID="LbUserName" runat="server"></asp:Label>![ <a href="/Login.aspx">Logout</a> ]</li>
                    </ul>
            </nav>
    </div>
    </div>

</header> <!-- End Header -->


<div class="content-wrapper">
  <div class="content-container container">
    <div class="page-title-row">
        <div class="title-container">
            <span class="section-heading">Government and Stakeholder Relations</span>
            <div align="right" id="logindisplay">
            </div>
        </div>
    </div>
    <div>
        <h3>MONTHLY LOBBYING REPORT FORM</h3>
     </div>

        <div class="content-container">

        <div class="form-area">
            <form class="form-horizontal"  id="lobbyingReport" role="form"  runat="server" method="post">
                

        <asp:LinkButton ID="LbDownload" runat="server" OnClick="LbDownload_Click">Download responses</asp:LinkButton>
&nbsp;|
        <asp:LinkButton ID="LbUsers" runat="server" OnClick="LbUsers_Click">Manage Users</asp:LinkButton>
    
                <br />
                <br />
                <br />
    
    <div class="form-group">
                    <label for="TbCCID" class="control-label col-md-offset-1 col-md-3">Name or CCID</label>
                    <div class="col-md-8">
                        <input runat="server" type="text" class="form-control" id="TbCCID" placeholder="Please enter the CCID" name="TbCCID"  style="height:2em">
                    </div>
        </div>
        <div class="form-group">
                    <label for="TbStartDate" class="control-label col-md-offset-1 col-md-3">Start Date</label>
                    <div class="col-md-8">
                        <input runat="server" type="text" class="form-control" id="TbStartDate" placeholder="Please enter the start date" name="TbStartDate"  style="height:2em">
                    </div>
        </div>
        <div class="form-group">
                    <label for="TbEndDate" class="control-label col-md-offset-1 col-md-3">End Date</label>
                    <div class="col-md-8">
                        <input runat="server" type="text" class="form-control" id="TbEndDate" placeholder="Please enter the end date" name="TbEndDate"  style="height:2em">
                    </div>
        </div>
<div class="form-group">
                    <label for="DdlComMethod" class="control-label col-md-offset-1 col-md-3">Communication Method</label>
                    <div class="col-md-8">
                        <asp:DropDownList class="form-control" ID="DdlComMethod" name="DdlComMethod" runat="server"></asp:DropDownList>
                    </div>
            </div>
<div class="form-group">

                    <label for="DdlGovOrder" class="control-label col-md-offset-1 col-md-3">Government Order</label>
                    <div class="col-md-8">
                        <asp:DropDownList class="form-control" ID="DdlGovOrder" name="DdlGovOrder" runat="server"></asp:DropDownList>
                    </div>
     </div>
                 <div class="form-group" >
                    <div class="col-md-offset-4 col-md-8">
                            <asp:Button ID="ButtonDisplay"  runat="server" class="btn btn-success" Text="Display Results" OnClick="ButtonDisplay_Click"  />
                        &nbsp;<asp:Button ID="ButtonDownload"  runat="server" class="btn btn-success" Text="Download Results" OnClick="ButtonDownload_Click" />
                    </div>
                 </div> 
                <div class="form-group" align="center">
                         <asp:GridView ID="GridViewResponses" runat="server" AutoGenerateColumns="False"  Width="90%" AllowPaging="True" OnPageIndexChanging="GridViewResponses_PageIndexChanging" PageSize="20" CssClass="table" OnSelectedIndexChanged="GridViewResponses_SelectedIndexChanged">
                            <Columns>
                                <asp:BoundField DataField="Date" HeaderText="Date" 
                                    SortExpression="Date" >
                                <ControlStyle Width="10%"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="ComMethodName" HeaderText="Communication Method" 
                                    ReadOnly="True" SortExpression="ComMethodName" >
                                <ControlStyle Width="10%"/>
                                </asp:BoundField>


                                <asp:TemplateField HeaderText="Subject Matter" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <div style="word-wrap: break-word; width: 200px;">
                                            <%#Eval("SubjectMatter") %></div>
                                    </ItemTemplate>

<HeaderStyle Width="10%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                </asp:TemplateField>

                                <asp:BoundField DataField="GovOrderName" HeaderText="Government Order Name" 
                                    SortExpression="GovOrderName" >
                                <ControlStyle Width="10%"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="UofAMeetingParticipant" HeaderText="University of Alberta Meeting Participants" 
                                    SortExpression="UofAMeetingParticipant" >
                                <ControlStyle Width="10%"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalMeetingParticipant" HeaderText="External Meeting Participants" 
                                    SortExpression="ExternalMeetingParticipant" >
                                <ControlStyle Width="10%"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="MeetingRequestedBy" HeaderText="Meeting Requested By" 
                                    SortExpression="MeetingRequestedBy" >
                                <ControlStyle Width="10%"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="FullName" HeaderText="Recorded By" 
                                    SortExpression="FullName" >
                                <ControlStyle Width="10%"/>
                                </asp:BoundField>
                            </Columns>
                                <HeaderStyle BackColor="#5CB85C" ForeColor="White"/>
                             <RowStyle Wrap="True" />
                            </asp:GridView>
                         <br />

                         <asp:Label class="control-label col-md-offset-1 col-md-3" ID="lbError" runat="server" style="color: #FF0000"></asp:Label>
                </div> 

            </form>
            </div>
            </div>
             </div>
            </div>
    <div class="bottom-footer-row ga_ualberta_footer"> <!-- Footer Bottom Row Start -->
      <div class="footer-container container"> <!-- Footer Container Start -->
        <div class="footer-inner row-fluid"> <!-- Footer Inner Start -->
        
          <div class="span12 footer-copyright">
            <ul class="footer-quick-links">
              <li><a href="http://uofa.ualberta.ca/contactualberta">Contact Us</a></li>
              <li><a href="https://www.surveymonkey.com/s/ualberta-web-feedback">Feedback</a></li>
              <li><a href="http://calendar.ualberta.ca">University Calendar</a></li>
              <li><a href="http://www.careers.ualberta.ca/">Careers</a></li>
              <li><a href="http://www.rms.ualberta.ca/en/EmergNumbers.aspx">Emergency</a></li>
              <li><a href="https://policiesonline.ualberta.ca/PoliciesProcedures/Pages/default.aspx">Policies</a></li>
              <li><a href="http://weather.gc.ca/city/pages/ab-50_metric_e.html" target="_blank">Weather</a></li>
              <li><a href="http://www.edmonton.ca/" target="_blank">Edmonton</a></li>
              <li><a href="http://uofa.ualberta.ca/privacy">Privacy</a></li>
              <li><a href="http://uofa.ualberta.ca/keepintouch">Keep in Touch</a></li>
              <li><a href="#">Back to Top</a></li>
            </ul>
            <p>
              &copy; <script>document.write(new Date().getFullYear())</script> University of Alberta 116 St. and 85 Ave., <br class="visible-phone" />Edmonton, AB, Canada T6G 2R3
            </p>
          </div>
        </div> <!-- Footer Inner End -->
      </div>  <!-- Footer Container End -->
    </div> <!-- Footer Bottom Row End -->
   <!-- Footer End -->
</body>
</html>                                 		