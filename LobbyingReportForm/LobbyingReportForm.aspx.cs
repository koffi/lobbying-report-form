﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LobbyingReportForm.DBACCESS;
using LobbyingReportForm.STRUCTURES;


namespace LobbyingReportForm
{
    public partial class LobbyingReportForm : System.Web.UI.Page
    {
        User TheUser = new User();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Context.User.Identity.IsAuthenticated)
            {
                Response.Redirect("Login.aspx");
            }

            TheUser = (new Entities()).GetUserByCCID(Context.User.Identity.Name);
            
            Page.DataBind();

            LbUserName.Text = TheUser.FirstName + " " + TheUser.LastName;

            DdlComMethod.DataSource = (new Entities()).ComMethod_GetAll();
            DdlComMethod.DataValueField = "ComMethodID";
            DdlComMethod.DataTextField = "ComMethodName";
            DdlComMethod.DataBind();

            DdlGovOrder.DataSource = (new Entities()).GovOrder_GetAll();
            DdlGovOrder.DataValueField = "GovOrderID";
            DdlGovOrder.DataTextField = "GovOrderName";
            DdlGovOrder.DataBind();
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {

            if (
                datepicker.Value != null &&
                DdlComMethod.SelectedValue != null &&
                DdlGovOrder.SelectedValue != null &&
                TbGovOfficialDept.Value != null &&
                TbGovOfficialName.Value != null &&
                TbMeetingRequestedBy.Value != null &&
                TbGovOfficialPosition.Value != null 
                )
            {

                    Response resp = new Response();

                    //resp.Date = Convert.ToDateTime(datepicker.SelectedDate);
                    resp.Date = Convert.ToDateTime(datepicker.Value);
                    resp.ComMethodID = Convert.ToInt16(DdlComMethod.SelectedValue);
                    resp.GovOrderID = Convert.ToInt16(DdlGovOrder.SelectedValue);
                    resp.GovOfficialDept = TbGovOfficialDept.Value;
                    resp.GovOfficialName = TbGovOfficialName.Value;
                    resp.MeetingRequestedBy = TbMeetingRequestedBy.Value;
                    resp.UofAMeetingParticipant = TbUofAMeetingParticipant.Value;
                    resp.ExternalMeetingParticipant = TbExternalMeetingParticipant.Value;
                    resp.Comments = TbComments.Value;
                    resp.SubjectMatter = TbSubjectMatter.Value;
                    resp.GovPosition = TbGovOfficialPosition.Value;
                    resp.UserID = TheUser.UserID;

                    (new Entities()).InsertResponse(resp);

                    Response.Redirect("Submitted.aspx");
            }
            else
            {
                LblError.Text = "Please enter all required fields.";
            }
        }


    }
}