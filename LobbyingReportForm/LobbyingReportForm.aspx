﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LobbyingReportForm.aspx.cs" Inherits="LobbyingReportForm.LobbyingReportForm" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Monthly Lobbying Report Form</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <link href="css/bootstrap-theme.min.css" rel="stylesheet" />
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.validate.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">

  <script>
      $(function () {
          var StartDate = new Date();
          $("#datepicker").datepicker();
          $("#datepicker").datepicker('setDate', StartDate);

      });
  </script>
<script>
    $(document).ready(function () {


        $('#lobbyingReport').validate(
            {
                submitHandler: function (form) {
                    if (confirm("You are about to submit, please recheck the form fields?")) {
                        form.submit();
                    }
                },

                rules: {
                    datepicker: { required: true },
                    DdlComMethod: { required: true },
                    DdlGovOrder: { required: true },
                    TbGovOfficialName: { required: true, minlength: 2 },
                    TbGovOfficialDept: { required: true },
                    TbGovOfficialPosition: { required: true },
                    TbSubjectMatter: { required: true },
                    TbMeetingRequestedBy: { required: true, minlength: 2 },
                },

                messages: {
                    datepicker: "Please Select a Date",
                    DdlComMethod: "Please Select Method of Communication",
                    DdlGovOrder: "Please Select Order of Government",
                    TbGovOfficialName: "Please Enter Name of Government Official (minimum of 2 characters)",
                    TbGovOfficialDept: "Please Enter the Department of Government Official",
                    TbGovOfficialPosition: "Please Enter Position of Government Official",
                    TbSubjectMatter: "Please Enter Subject Matter",
                    TbMeetingRequestedBy: "Please Enter Organizers Name (minimum 2 characters)",
                },
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                }


            });
    });
</script>
    <link href="css/Site.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            font-size: medium;
            text-align: justify;
        }
        input[type="text"] {
            height: 2em;
        }
    </style>
    <link href="https://www.ualberta.ca/~publicas/uofa/css/homepage-fluid.css" rel="stylesheet">
    <link href="https://uofa.ualberta.ca/design/css/framework.css" rel="stylesheet">
</head>
<body>
<header> 
 <div class="blade-container ga_ualberta_blade" style="height:inherit;"><!-- Start Blade -->
      <div class="blade-inner">
        <a class="brand" href="#"></a>
        <nav class="blade-navigation">
          <ul class="quick-links-container">
            <li id="ql" class="dropdown">
              <a href="#" id="ql-toggle" class="btn btn-grey btn-small hidden-desktop">
                <span class="quick-links-text">
                  Quick Links <b class="caret"></b>
                </span>
                <span class="icon-bar quick-links-button"></span>
                <span class="icon-bar quick-links-button"></span>
                <span class="icon-bar quick-links-button"></span>
              </a>
              <ul class="quick-links" role="menu" aria-labelledby="ql-toggle">
              <li role="menuitem"><a href="http://myualberta.ualberta.ca">My UAlberta</a></li>
                <li role="menuitem"><a href="http://webapps.srv.ualberta.ca/search/">Find a Person</a></li>
                <li role="menuitem"><a href="http://www.onecard.ualberta.ca/">ONEcard</a></li>
                <li role="menuitem"><a href="https://www.beartracks.ualberta.ca/">Bear Tracks</a></li>
                <li role="menuitem"><a href="http://www.campusmap.ualberta.ca/">Maps</a></li>
                <li role="menuitem"><a href="http://apps.ualberta.ca/">Email &amp; Apps</a></li>
                <li role="menuitem"><a href="https://eclass.srv.ualberta.ca/portal/">eClass</a></li>
                <li role="menuitem"><a href="http://www.library.ualberta.ca/">Libraries</a></li>
              </ul>
            </li>
          </ul>
        </nav>
        <div class="blade-search">
           <i class="icon-search"></i>
           <input type="search" class="search-query" placeholder="search">
           <button type="submit" class="btn btn-grey btn-small global-search-btn">Search</button>
        </div>
      </div>
    </div> <!-- End Blade -->
    <div class="top-header row-fluid">
      <div class="banner-inner">
        <h1 class="hp-h1">
          <span class="identifier"></span>
          <span class="site-title">University of Alberta</span>
        </h1>
        <div class="banner-logo" style="display: block;">
          <a href="http://www.ualberta.ca">
            <img src="http://www.ualberta.ca/~publicas/uofa/img/logo-reverse.svg" class="site-logo" alt="University of Alberta" />
          </a>
        </div>
            <nav class="audience-navigation" style="right: 20%;">
                    <ul>
                    <li role="menuitem" class="audience-link">Welcome <asp:Label ID="LbUserName" runat="server"></asp:Label>![ <a href="/Login.aspx">Logout</a> ]</li>
                    </ul>
            </nav>
    </div>
    </div>
</header> <!-- End Header -->

<div class="content-wrapper">
  <div class="content-container container">
    <div class="page-title-row">
        <div class="title-container">
            <span class="section-heading">Government and Stakeholder Relations</span>
            <div align="right" id="logindisplay">
            </div>
        </div>
    </div>
    <div>
        <h3>MONTHLY LOBBYING REPORT FORM</h3>
        <p class="text-justify">All government meetings (federal, provincial, municipal) at any level are tracked by the Office of Government and Stakeholder Relations. All reports must include the name(s) and title(s) of the highest ranking official(s) and all other staff in attendance.  </p>
        <p class="text-justify">Monthly lobbying reports must be recorded and submitted to the Office of Government and Stakeholder Relations no later than the 10th day of the following month of the government meeting (e.g. government meetings in May to be recorded and submitted by June 10)</p>
    </div>
        <div class="form-area">
            <form class="form-horizontal"  id="lobbyingReport" role="form"  runat="server" method="post">

                    <div class="form-group">
                    <label for="datepicker" class="control-label col-md-offset-1 col-md-3">Date*</label>
                    <div class="col-md-8">
                        <input runat="server" type="text" class="form-control" id="datepicker" placeholder="Please enter the date" name="datepicker"  style="height:2em">
                    </div>
                </div>
                <div class="form-group">
                    <label for="DdlComMethod" class="control-label col-md-offset-1 col-md-3">Method of communication*</label>
                    <div class="col-md-8">
                        <asp:DropDownList class="form-control" ID="DdlComMethod" name="DdlComMethod" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label for="DdlGovOrder" class="control-label col-md-offset-1 col-md-3">Order of Government*</label>
                    <div class="col-md-8">
                        <asp:DropDownList class="form-control" ID="DdlGovOrder" name="DdlGovOrder" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label for="TbGovOfficialName" class="control-label col-md-offset-1 col-md-3">Name of Government Official*</label>
                    <div class="col-md-8">
                        <input runat="server" type="text" class="form-control" id="TbGovOfficialName" placeholder="i.e John Doe" name="TbGovOfficialName"  style="height:2em">
                    </div>
                </div>
                <div class="form-group">
                    <label for="TbGovOfficialDept" class="control-label col-md-offset-1 col-md-3">Department of Government Official'*</label>
                    <div class="col-md-8">
                        <input runat="server" type="text" class="form-control" id="TbGovOfficialDept" placeholder="" name="TbGovOfficialDept"  style="height:2em">
                    </div>
                </div>
                <div class="form-group">
                    <label for="TbGovOfficialPosition" class="control-label col-md-offset-1 col-md-3">Position of Government Official'*</label>
                    <div class="col-md-8">
                        <input runat="server" type="text" class="form-control" id="TbGovOfficialPosition" placeholder="i.e. MP, MLA, DM, ADM, Mayor, Councillor, Senator, etc" name="TbGovOfficialPosition"  style="height:2em">
                    </div>
                </div>
                <div class="form-group">
                    <label for="TbSubjectMatter" class="control-label col-md-offset-1 col-md-3">Subject matter*</label>
                    <div class="col-md-8">
                        <textarea runat="server" class="form-control" rows="5"  id="TbSubjectMatter" placeholder="i.e. grant, contribution or other financial benefits, policies or program, regulation, legislative proposal, etc" name="TbSubjectMatter"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="TbUofAMeetingParticipant" class="control-label col-md-offset-1 col-md-3">University of Alberta meeting participants</label>
                    <div class="col-md-8">
                        <input runat="server" type="text" class="form-control" id="TbUofAMeetingParticipant" placeholder="i.e. lobbyist"  style="height:2em">
                    </div>
                </div>
                <div class="form-group">
                    <label for="TbExternalMeetingParticipant" class="control-label col-md-offset-1 col-md-3">External meeting participants</label>
                    <div class="col-md-8">
                        <textarea runat="server" class="form-control" rows="5" id="TbExternalMeetingParticipant"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="TbMeetingRequestedBy" class="control-label col-md-offset-1 col-md-3">Meeting Requested By*</label>
                    <div class="col-md-8">
                        <input runat="server" type="text" class="form-control" id="TbMeetingRequestedBy" placeholder="i.e. John Doe" name="TbMeetingRequestedBy"  style="height:2em">
                    </div>
                </div>
                <div class="form-group">
                    <label for="TbComments" class="control-label col-md-offset-1 col-md-3">Comments</label>
                    <div class="col-md-8">
                        <textarea runat="server" class="form-control" rows="5" id="TbComments"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8">
                        <asp:Button ID="ButtonSubmit" runat="server" class="btn btn-success" Text="Submit" OnClick="ButtonSubmit_Click"/>
                    &nbsp;
                    </div>
                    <div class="text-center">
                        <asp:Label ID="LblError" runat="server" Text=""></asp:Label>
                        <br />
                        <br />
                        <br />
                        <span style="text-align: center">Please contact the Office of Government and Stakeholder Relations at <a href="mailto:government.relations@ualberta.ca" target="_blank">government.relations@ualberta.ca</a>&nbsp;if you have any questions or concerns</span></div>
                </div>

            </form>
            </div>
      </div>
    </div>

    <div class="bottom-footer-row ga_ualberta_footer"> <!-- Footer Bottom Row Start -->
      <div class="footer-container container"> <!-- Footer Container Start -->
        <div class="footer-inner row-fluid"> <!-- Footer Inner Start -->
        
          <div class="span12 footer-copyright">
            <ul class="footer-quick-links">
              <li><a href="http://uofa.ualberta.ca/contactualberta">Contact Us</a></li>
              <li><a href="https://www.surveymonkey.com/s/ualberta-web-feedback">Feedback</a></li>
              <li><a href="http://calendar.ualberta.ca">University Calendar</a></li>
              <li><a href="http://www.careers.ualberta.ca/">Careers</a></li>
              <li><a href="http://www.rms.ualberta.ca/en/EmergNumbers.aspx">Emergency</a></li>
              <li><a href="https://policiesonline.ualberta.ca/PoliciesProcedures/Pages/default.aspx">Policies</a></li>
              <li><a href="http://weather.gc.ca/city/pages/ab-50_metric_e.html" target="_blank">Weather</a></li>
              <li><a href="http://www.edmonton.ca/" target="_blank">Edmonton</a></li>
              <li><a href="http://uofa.ualberta.ca/privacy">Privacy</a></li>
              <li><a href="http://uofa.ualberta.ca/keepintouch">Keep in Touch</a></li>
              <li><a href="#">Back to Top</a></li>
            </ul>
            <p>
              &copy; <script>document.write(new Date().getFullYear())</script> University of Alberta 116 St. and 85 Ave., <br class="visible-phone" />Edmonton, AB, Canada T6G 2R3
            </p>
          </div>
        </div> <!-- Footer Inner End -->
      </div>  <!-- Footer Container End -->
    </div> <!-- Footer Bottom Row End -->
   <!-- Footer End -->
</body>
</html>                                 		